#!/bin/bash

# Initialize the deck
suits=("\xE2\x99\xA5" "\xE2\x99\xA6" "\xE2\x99\xA3" "\xE2\x99\xA0")
ranks=("2" "3" "4" "5" "6" "7" "8" "9" "10" "J" "Q" "K" "A")

# Function to get a random card
get_random_card() {
  suit=${suits[$((RANDOM % 4))]}
  rank=${ranks[$((RANDOM % 13))]}

  echo "$rank $suit"
}

# Function to calculate the value of a hand
calculate_hand_value() {
  local hand=("$@")
  local value=0
  local has_ace=false

  for card in "${hand[@]}"; do
    rank="${card%% *}"
    case "$rank" in
      "K" | "Q" | "J")
        value=$((value + 10))
        ;;
      "A")
        value=$((value + 11))
        has_ace=true
        ;;
      *)
        value=$((value + $rank))
        ;;
    esac
  done

  if $has_ace && [ $value -gt 21 ]; then
    value=$((value - 10)) 
  fi

  echo "$value"
}

# Initialize player's and dealer's hands
player_hand=()
dealer_hand=()

# Deal the initial cards
player_hand+=("$(get_random_card)")
dealer_hand+=("$(get_random_card)")

# Player's turn
while true; do
  # Display initial hand
  ph=${player_hand[@]}
  dh=${dealer_hand[0]}
  
  # Check for bust
  player_value=$(calculate_hand_value "${player_hand[@]}")
  if [ $player_value -gt 21 ]; then
    ph=${player_hand[@]}
    dh=${dealer_hand[0]}
    dialog --msgbox "Blackjack\n\nYour hand: `printf "$ph"`\nDealer's face-up card: `printf "$dh"`\n\nBust! You lose." 15 60
    exit
  fi

  choice=$(dialog --stdout --title "Blackjack" --menu "\nYour hand: `printf "$ph"`\nDealer's hand: `printf "$dh"`\nChoose the option?" 15 60 5 1 "Stand" 2 "Hit") 

  if [ "$choice" -eq 1 ]; then
    break # Player chose to stand
  fi


  # Hit: Deal a new card to the player
  player_hand+=("$(get_random_card)")

done

# Dealers turn
while true; do
  # Dealer hits until hand value is at least 17
  dealer_value=$(calculate_hand_value "${dealer_hand[@]}")
  if [ $dealer_value -ge 17 ]; then
    break
  fi

  # Hit: Deal a new card to the dealer
  dealer_hand+=("$(get_random_card)")
done

# Determine the winner
ph=${player_hand[@]}
dh=${dealer_hand[@]}
dialog --msgbox "Blackjack\n\nYour hand: `printf "$ph"` \nDealer's hand: `printf "$dh"`\n\nYour hand value: $player_value\nDealer's hand value: $dealer_value" 15 60

if [ $player_value -gt 21 ]; then
  dialog --msgbox "Blackjack\n\nBust! You lose." 10 30
elif [ $dealer_value -gt 21 ] || [ $player_value -gt $dealer_value ]; then
  dialog --msgbox "Blackjack\n\nCongratulations! You win." 10 30
elif [ $player_value -eq $dealer_value ]; then
  dialog --msgbox "Blackjack\n\nIt's a push (tie)." 10 30
else
  dialog --msgbox "Blackjack\n\nDealer wins." 10 30
fi

