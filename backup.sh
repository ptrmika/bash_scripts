#!/bin/bash
#echo "$1"
# so far the copying to the google drive is not possible because there is not enough tokens available
if [ "$#" -eq 0 ]
then
  # this is a specific to the user folder(change accordingly)
  drive_dir=/run/user/1000/gvfs/google-drive\:host\=gmail.com\,user\=piotrmika97/0AGuIfk_kngPeUk9PVA/
  cd $drive_dir
  g_names=()
  g_act=()
  # gio is used to extract real names of files and folders in Google Drive
  out=($(gio list -a "standard::display-name" | grep '(directory)' | sed "s/\t[0-9]*\t(directory)//g" | sed "s/\tstandard::display-name=/ /g"))
  
  # creating dialog for choice of folders(only)
  let n=${#out[@]}/2-1
  for i in $(seq 0 $n)
  do
    g_act[i]="${out[2*$i]}"
    g_names[i]="${out[2*$i+1]}"
  done

  cmd=()
  let i=1
  for el in "${g_names[@]}"
  do
    cmd+=("$i")
    cmd+=("$el")
    ((i++))
  done
  choice=$(dialog --stdout --menu "Choose directory to copy into" 15 50 50 "${cmd[@]}")
  dir_to=$drive_dir${g_act[$choice-1]}

else
  # check if correct option was used
  if [ "$1" == "-d" ]
  then
    dir_to=$(dialog --dselect /home/piotrmika/ 30 60 --stdout)
  else
    echo "Invalid option\n"
    exit 126
  fi
fi

dir_from=$(dialog --fselect /home/piotrmika/ 30 60 --stdout)


#sudo time gsync -c -r -t -p -o -g -v --progress --delete -l -s dir_from dir_to
rsync -a -c -r -t -p -o -g -v --delete --info=progress2 $dir_from $dir_to
#rsync -c -r -t -p -o -g -v --delete --info=progress2 $dir_from $dir_to
